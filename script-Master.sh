#!/bin/bash

if [ "$#" -ne "5" ]; then
	echo "ERROR: script needs five arguments, where:"
	echo
	echo "1. IP of master"
	echo "2. IP of jobtracker"
	echo "3 to 5. IP of Data1 to Data3"
	echo
	exit 1
else
	IPm=$1
	IPjt=$2
	IP1=$3
	IP2=$4
	IP3=$5
fi

bash script-HMN.sh $1 $2 $3 $4 $5

ssh jobtracker
bash script-HMN.sh $1 $2 $3 $4 $5

ssh Data1
bash script-HMN.sh $1 $2 $3 $4 $5

ssh Data2
bash script-HMN.sh $1 $2 $3 $4 $5

ssh Data3
bash script-HMN.sh $1 $2 $3 $4 $5