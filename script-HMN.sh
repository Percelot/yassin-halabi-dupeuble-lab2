#!/bin/bash

if [ "$#" -ne "5" ]; then
	echo "ERROR: script needs five arguments, where:"
	echo
	echo "1. IP of master"
	echo "2. IP of jobtracker"
	echo "3 to 5. IP of Data1 to Data3"
	echo
	exit 1
else
	IPm=$1
	IPjt=$2
	IP1=$3
	IP2=$4
	IP3=$5
fi

# configuring multi-node cluster
# http://www.michael-noll.com/tutorials/running-hadoop-on-ubuntu-linux-multi-node-cluster/

sudo chmod 777 /etc/hosts

sudo echo '127.0.0.1 localhost' > /etc/hosts
sudo echo $IPm ' master' >> /etc/hosts
sudo echo $IPjt ' jobtracker' >> /etc/hosts
sudo echo $IP1 ' Data1' >> /etc/hosts
sudo echo $IP2 ' Data2' >> /etc/hosts
sudo echo $IP3 ' Data3' >> /etc/hosts
sudo echo >> /etc/hosts
sudo echo '# The following lines are desirable for IPv6 capable hosts' >> /etc/hosts
sudo echo '::1 ip6-localhost ip6-loopback' >> /etc/hosts
sudo echo 'fe00::0 ip6-localnet' >> /etc/hosts
sudo echo 'fe00::0 ip6-mcastprefix' >> /etc/hosts
sudo echo 'fe02::1 ip6-allnodes' >> /etc/hosts
sudo echo 'fe02::2 ip6-allrouters' >> /etc/hosts

sudo cp hadoop-multi/* $HADOOP_PREFIX/etc/hadoop

