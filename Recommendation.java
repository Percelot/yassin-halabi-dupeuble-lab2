import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.lang.System.*;

import org.apache.commons.lang.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class Recommendation extends Configured implements Tool {
 
  /**
   * Map class
   * Reads the graph of friends line by line
   * and emits all pairs of friends or 
   * pairs of second-degree friends. 
   */

  public static class Map extends MapReduceBase
    implements Mapper<LongWritable, Text, IntWritable, Text> {
    
    // value is a line
    public void map(LongWritable key, Text value, OutputCollector<IntWritable,Text> output, Reporter reporter) throws IOException {
      String line = value.toString();
      // split value between the first user and his friends (separated by TAB in input file)
      String[] userFriends = line.split("\t");
      // verification that there are 2 elements in the previously created table
      if (userFriends.length == 2) {
        // user is the user ID of the line we consider ; userKey = corresponding key for output
        String user = userFriends[0];
        IntWritable userKey = new IntWritable(Integer.parseInt(user));
        // friends is the list of friends of user
        String[] friends = userFriends[1].split(",");
        
        // friend1 and friend2 will contain all the friends in friends
          // creation of keys and values for output
        String friend1;
        IntWritable friend1Key = new IntWritable();
        Text friend1Value = new Text();
        String friend2;
        IntWritable friend2Key = new IntWritable();
        Text friend2Value = new Text();

        // double for loop to get all direct friends of user (pairs of friends) and all pairs of second-degree friends
        for (int i = 0; i < friends.length; i++) {
          friend1 = friends[i];
          String str0;
          str0 = friend1 + ",0";
          friend1Value.set(str0);
          output.collect(userKey, friend1Value);   // pairs of friends

          friend1Key.set(Integer.parseInt(friend1));
          String str1;
          str1 = friend1 + ",1";
          friend1Value.set(str1);
          for (int j = i+1; j < friends.length; j++) {
            friend2 = friends[j];
            friend2Key.set(Integer.parseInt(friend2));
            String str2;
            str2 = friend2 + ",1";
            friend2Value.set(str2);
            output.collect(friend1Key, friend2Value);   // pairs of second-degree friends
            output.collect(friend2Key, friend1Value);   // symmetric
          }
        }
      }
    }
  }

  /**
   * Reduce class
   * Receives ('userID1',(C,'userID2')) where C = 1 if second-degree friends, C = 0 if already friends
   * Outputs the list of 10 best recommendations
   */  

  public static class Reduce extends MapReduceBase
    implements Reducer<IntWritable, Text, IntWritable, Text> {
   
    public void reduce(IntWritable key, Iterator<Text> values,
                       OutputCollector<IntWritable, Text> output,
                       Reporter reporter) throws IOException {

      // value will contain values, in a well-formated way
        // e.g. value = ['user2','0']
      String foo = new String();
      String[] value;
      // hash will contain a dictionary listing second-degree friends and number of friends in common with key user
        // e.g. hash = <('user1',1),('user2',-1),('user3',4)>
      HashMap<String, Integer> hash = new HashMap<String, Integer>();
      
      // filling hash according to values
      while (values.hasNext()){
        foo = values.next().toString();
        System.out.println(foo);
        value = foo.split(","); // splitting values in a table with two entries : value[0] = 0 or 1, value[1] = friend ID
        if(value.length==2){
          if (value[1].equals("0")) { // already friends
            hash.put(value[0], -1); 
          } 
          else if (value[1].equals("1")) {  // one friend in common
            // add one to the number of friends in common if not already a friend
            if (hash.containsKey(value[0])) {
              if (hash.get(value[0]) != -1) {
                hash.put(value[0], hash.get(value[0]) + 1);
              }
            } 
            else {
              hash.put(value[0], 1);
            }
          }
        }
        else{
          Text val = new Text();
          val.set("0");
          output.collect(key,val);
        }
      }
      // list will contain hash without pairs of friends
      ArrayList<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>();
      for (Entry<String, Integer> entry : hash.entrySet()) {
        if (entry.getValue() != -1) {   // delete pairs of friends
          list.add(entry);
        }
      }
      // sort key-value pairs in the list by number of common friends
      Collections.sort(list, new Comparator<Entry<String, Integer>>() {
        public int compare(Entry<String, Integer> e1, Entry<String, Integer> e2) {
          return e2.getValue().compareTo(e1.getValue());
        }
      });

      // output 10 first recommendations for each key
      int MAX_RECOMMENDATION_COUNT = 10;  
      // best will contain 10 first elements of list
      ArrayList<String> best = new ArrayList<String>();
      for (int i = 0; i < Math.min(MAX_RECOMMENDATION_COUNT, list.size()); i++) {
        best.add(list.get(i).getKey());
      }
      //if (Arrays.asList("917","8907","8997","9023","9037","9021","9027","9995","9996","9997").contains(key)){
        output.collect(key, new Text(StringUtils.join(best, ","))); // output : 'userKey' 'user3,user1' 
      //}
    }
  }

  static int printUsage() {
    System.out.println("Recommendation [-m <maps>] [-r <reduces>] <input> <output>");
    ToolRunner.printGenericCommandUsage(System.out);
    return -1;
  }
 
  /**
   * The main driver for map/reduce program.
   * Invoke this method to submit the map/reduce job.
   * @throws IOException When there is communication problems with the
   *                     job tracker.
   */
  public int run(String[] args) throws Exception {
    JobConf conf = new JobConf(getConf(), Recommendation.class);
    conf.setJobName("Recommendation");
 
    // the keys are userIDs (ints)
    conf.setOutputKeyClass(IntWritable.class);
    // the values are lists of users (strings)
    conf.setOutputValueClass(Text.class);
   
    conf.setMapperClass(Map.class);        
    conf.setCombinerClass(Reduce.class);
    conf.setReducerClass(Reduce.class);
   
    List<String> other_args = new ArrayList<String>();
    for(int i=0; i < args.length; ++i) {
      try {
        if ("-m".equals(args[i])) {
          conf.setNumMapTasks(Integer.parseInt(args[++i]));
        } else if ("-r".equals(args[i])) {
          conf.setNumReduceTasks(Integer.parseInt(args[++i]));
        } else {
          other_args.add(args[i]);
        }
      } catch (NumberFormatException except) {
        System.out.println("ERROR: Integer expected instead of " + args[i]);
        return printUsage();
      } catch (ArrayIndexOutOfBoundsException except) {
        System.out.println("ERROR: Required parameter missing from " +
                           args[i-1]);
        return printUsage();
      }
    }
    // Make sure there are exactly 2 parameters left.
    if (other_args.size() != 2) {
      System.out.println("ERROR: Wrong number of parameters: " +
                         other_args.size() + " instead of 2.");
      return printUsage();
    }
    FileInputFormat.setInputPaths(conf, other_args.get(0));
    FileOutputFormat.setOutputPath(conf, new Path(other_args.get(1)));
       
    JobClient.runJob(conf);
    return 0;
  }
 
 
  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new Configuration(), new Recommendation(), args);
    System.exit(res);
  }

}





