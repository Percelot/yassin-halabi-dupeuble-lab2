#!/bin/bash

# installing Java 7
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java7-installer

echo 'export JAVA_HOME=/usr/lib/jvm/java-7-oracle' >> ~/.profile
echo 'export PATH=$PATH:$JAVA_HOME/bin' >> ~/.profile

source ~/.profile

# installing Hadoop 2.7.2
wget http://apache.mirror.gtcomm.net/hadoop/common/hadoop-2.7.2/hadoop-2.7.2.tar.gz
sudo tar -xf hadoop-2.7.2.tar.gz -C /usr/local

echo 'export HADOOP_PREFIX=/usr/local/hadoop-2.7.2' >> ~/.profile
echo 'export PATH=$HADOOP_PREFIX/bin:$PATH' >> ~/.profile

source ~/.profile

# defining parameters in etc/hadoop/hadoop-env.sh
sudo cp hadoop-conf/* $HADOOP_PREFIX/etc/hadoop/

# configuring single-node cluster 
# http://www.michael-noll.com/tutorials/running-hadoop-on-ubuntu-linux-single-node-cluster/

# allowing ssh
ssh-keygen -t rsa -P "" -f ~/.ssh/id_rsa
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

# creation of hdfs directory
sudo mkdir -p /app/hadoop/tmp
sudo chmod 777 /app/hadoop/tmp
$HADOOP_PREFIX/bin/hadoop namenode -format
