#!/bin/bash

for run in $(seq 1 10); do
	/usr/bin/time -po logtime hadoop jar hadoop-examples-1.1.1.jar wordcount input output
	grep 'real' logtime >> plot
	hdfs dfs -rm -r output/
done